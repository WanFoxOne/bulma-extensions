const path = require('path')

module.exports = function nuxtBulmaExtensions(options) {
  // Add CSS
  this.options.css.push('bulma-extensions/dist/css/bulma-extensions.min.css')
}

module.exports.meta = require('./package.json')
